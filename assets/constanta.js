const countries = {
  'AF': {
    ext: 93,
    img: 'af.png',
    label: `Afghanistan`
  },
  'AL': {
    ext: 355,
    img: 'al.png',
    label: `Albania`
  },
  'DZ': {
    ext: 213,
    img: 'dz.png',
    label: `Algeria`
  },
  'AS': {
    ext: 1684,
    img: 'as.png',
    label: `American Samoa`
  },
  'AD': {
    ext: 376,
    img: 'ad.png',
    label: `Andorra`
  },
  'AO': {
    ext: 244,
    img: 'ao.png',
    label: `Angola`
  },
  'AI': {
    ext: 1264,
    img: 'ai.png',
    label: `Anguilla`
  },
  'AQ': {
    ext: 672,
    img: 'aq.png',
    label: `Antarctica`
  },
  'AG': {
    ext: 1268,
    img: 'ag.png',
    label: `Antigua and Barbuda`
  },
  'AR': {
    ext: 54,
    img: 'ar.png',
    label: `Argentina`
  },
  'AM': {
    ext: 374,
    img: 'am.png',
    label: `Armenia`
  },
  'AW': {
    ext: 297,
    img: 'aw.png',
    label: `Aruba`
  },
  'AU': {
    ext: 61,
    img: 'au.png',
    label: `Australia`
  },
  'AT': {
    ext: 43,
    img: 'at.png',
    label: `Austria`
  },
  'AZ': {
    ext: 994,
    img: 'az.png',
    label: `Azerbaijan`
  },
  'BS': {
    ext: 1242,
    img: 'bs.png',
    label: `Bahamas`
  },
  'BH': {
    ext: 973,
    img: 'bh.png',
    label: `Bahrain`
  },
  'BD': {
    ext: 880,
    img: 'bd.png',
    label: `Bangladesh`
  },
  'BB': {
    ext: 1246,
    img: 'bb.png',
    label: `Barbados`
  },
  'BY': {
    ext: 375,
    img: 'by.png',
    label: `Belarus`
  },
  'BE': {
    ext: 32,
    img: 'be.png',
    label: `Belgium`
  },
  'BZ': {
    ext: 501,
    img: 'bz.png',
    label: `Belize`
  },
  'BJ': {
    ext: 229,
    img: 'bj.png',
    label: `Benin`
  },
  'BM': {
    ext: 1441,
    img: 'bm.png',
    label: `Bermuda`
  },
  'BT': {
    ext: 975,
    img: 'bt.png',
    label: `Bhutan`
  },
  'BO': {
    ext: 591,
    img: 'bo.png',
    label: `Bolivia (Plurinational State of)`
  },
  'BQ': {
    ext: 599,
    img: 'bq.png',
    label: `Bonaire, Sint Eustatius and Saba`
  },
  'BA': {
    ext: 387,
    img: 'ba.png',
    label: `Bosnia and Herzegovina`
  },
  'BW': {
    ext: 267,
    img: 'bw.png',
    label: `Botswana`
  },
  'BV': {
    ext: 47,
    img: 'bv.png',
    label: `Bouvet Island`
  },
  'BR': {
    ext: 55,
    img: 'br.png',
    label: `Brazil`
  },
  'IO': {
    ext: 246,
    img: 'io.png',
    label: `British Indian Ocean Territory`
  },
  'VG': {
    ext: 1284,
    img: 'vg.png',
    label: `British Virgin Islands`
  },
  'BN': {
    ext: 673,
    img: 'bn.png',
    label: `Brunei Darussalam`
  },
  'BG': {
    ext: 359,
    img: 'bg.png',
    label: `Bulgaria`
  },
  'BF': {
    ext: 226,
    img: 'bf.png',
    label: `Burkina Faso`
  },
  'BI': {
    ext: 257,
    img: 'bi.png',
    label: `Burundi`
  },
  'CV': {
    ext: 238,
    img: 'cv.png',
    label: `Cabo Verde`
  },
  'KH': {
    ext: 855,
    img: 'kh.png',
    label: `Cambodia`
  },
  'CM': {
    ext: 237,
    img: 'cm.png',
    label: `Cameroon`
  },
  'CA': {
    ext: 1,
    img: 'ca.png',
    label: `Canada`
  },
  'KY': {
    ext: 1345,
    img: 'ky.png',
    label: `Cayman Islands`
  },
  'CF': {
    ext: 236,
    img: 'cf.png',
    label: `Central African Republic`
  },
  'TD': {
    ext: 235,
    img: 'td.png',
    label: `Chad`
  },
  'CL': {
    ext: 56,
    img: 'cl.png',
    label: `Chile`
  },
  'CN': {
    ext: 86,
    img: 'cn.png',
    label: `China`
  },
  'HK': {
    ext: 852,
    img: 'hk.png',
    label: `China, Hong Kong Special Administrative Region`
  },
  'MO': {
    ext: 853,
    img: 'mo.png',
    label: `China, Macao Special Administrative Region`
  },
  'CX': {
    ext: 61,
    img: 'cx.png',
    label: `Christmas Island`
  },
  'CC': {
    ext: 61,
    img: 'cc.png',
    label: `Cocos (Keeling) Islands`
  },
  'CO': {
    ext: 57,
    img: 'co.png',
    label: `Colombia`
  },
  'KM': {
    ext: 269,
    img: 'km.png',
    label: `Comoros`
  },
  'CG': {
    ext: 242,
    img: 'cg.png',
    label: `Congo`
  },
  'CK': {
    ext: 682,
    img: 'ck.png',
    label: `Cook Islands`
  },
  'CR': {
    ext: 506,
    img: 'cr.png',
    label: `Costa Rica`
  },
  'HR': {
    ext: 385,
    img: 'hr.png',
    label: `Croatia`
  },
  'CU': {
    ext: 53,
    img: 'cu.png',
    label: `Cuba`
  },
  'CW': {
    ext: 599,
    img: 'cw.png',
    label: `Curaçao`
  },
  'CY': {
    ext: 357,
    img: 'cy.png',
    label: `Cyprus`
  },
  'CZ': {
    ext: 420,
    img: 'cz.png',
    label: `Czechia`
  },
  'CI': {
    ext: 225,
    img: 'ci.png',
    label: `Côte d'Ivoire`
  },
  'KP': {
    ext: 850,
    img: 'kp.png',
    label: `Democratic People's Republic of Korea`
  },
  'CD': {
    ext: 243,
    img: 'cd.png',
    label: `Democratic Republic of the Congo`
  },
  'DK': {
    ext: 45,
    img: 'dk.png',
    label: `Denmark`
  },
  'DJ': {
    ext: 253,
    img: 'dj.png',
    label: `Djibouti`
  },
  'DM': {
    ext: 1767,
    img: 'dm.png',
    label: `Dominica`
  },
  'DO': {
    ext: 1,
    img: 'do.png',
    label: `Dominican Republic`
  },
  'EC': {
    ext: 593,
    img: 'ec.png',
    label: `Ecuador`
  },
  'EG': {
    ext: 20,
    img: 'eg.png',
    label: `Egypt`
  },
  'SV': {
    ext: 503,
    img: 'sv.png',
    label: `El Salvador`
  },
  'GQ': {
    ext: 240,
    img: 'gq.png',
    label: `Equatorial Guinea`
  },
  'ER': {
    ext: 291,
    img: 'er.png',
    label: `Eritrea`
  },
  'EE': {
    ext: 372,
    img: 'ee.png',
    label: `Estonia`
  },
  'SZ': {
    ext: 268,
    img: 'sz.png',
    label: `Eswatini`
  },
  'ET': {
    ext: 251,
    img: 'et.png',
    label: `Ethiopia`
  },
  'FK': {
    ext: 500,
    img: 'fk.png',
    label: `Falkland Islands (Malvinas)`
  },
  'FO': {
    ext: 298,
    img: 'fo.png',
    label: `Faroe Islands`
  },
  'FJ': {
    ext: 679,
    img: 'fj.png',
    label: `Fiji`
  },
  'FI': {
    ext: 358,
    img: 'fi.png',
    label: `Finland`
  },
  'FR': {
    ext: 33,
    img: 'fr.png',
    label: `France`
  },
  'GF': {
    ext: 594,
    img: 'gf.png',
    label: `French Guiana`
  },
  'PF': {
    ext: 689,
    img: 'pf.png',
    label: `French Polynesia`
  },
  'TF': {
    ext: 262,
    img: 'tf.png',
    label: `French Southern Territories`
  },
  'GA': {
    ext: 241,
    img: 'ga.png',
    label: `Gabon`
  },
  'GM': {
    ext: 220,
    img: 'gm.png',
    label: `Gambia`
  },
  'GE': {
    ext: 995,
    img: 'ge.png',
    label: `Georgia`
  },
  'DE': {
    ext: 49,
    img: 'de.png',
    label: `Germany`
  },
  'GH': {
    ext: 233,
    img: 'gh.png',
    label: `Ghana`
  },
  'GI': {
    ext: 350,
    img: 'gi.png',
    label: `Gibraltar`
  },
  'GR': {
    ext: 30,
    img: 'gr.png',
    label: `Greece`
  },
  'GL': {
    ext: 299,
    img: 'gl.png',
    label: `Greenland`
  },
  'GD': {
    ext: 1473,
    img: 'gd.png',
    label: `Grenada`
  },
  'GP': {
    ext: 590,
    img: 'gp.png',
    label: `Guadeloupe`
  },
  'GU': {
    ext: 1671,
    img: 'gu.png',
    label: `Guam`
  },
  'GT': {
    ext: 502,
    img: 'gt.png',
    label: `Guatemala`
  },
  'GG': {
    ext: 44,
    img: 'gg.png',
    label: `Guernsey`
  },
  'GN': {
    ext: 224,
    img: 'gn.png',
    label: `Guinea`
  },
  'GW': {
    ext: 245,
    img: 'gw.png',
    label: `Guinea-Bissau`
  },
  'GY': {
    ext: 592,
    img: 'gy.png',
    label: `Guyana`
  },
  'HT': {
    ext: 509,
    img: 'ht.png',
    label: `Haiti`
  },
  'HM': {
    ext: 672,
    img: 'hm.png',
    label: `Heard Island and McDonald Islands`
  },
  'VA': {
    ext: 3906,
    img: 'va.png',
    label: `Holy See`
  },
  'HN': {
    ext: 504,
    img: 'hn.png',
    label: `Honduras`
  },
  'HU': {
    ext: 36,
    img: 'hu.png',
    label: `Hungary`
  },
  'IS': {
    ext: 354,
    img: 'is.png',
    label: `Iceland`
  },
  'IN': {
    ext: 91,
    img: 'in.png',
    label: `India`
  },
  'ID': {
    ext: 62,
    img: 'id.png',
    label: `Indonesia`
  },
  'IR': {
    ext: 98,
    img: 'ir.png',
    label: `Iran (Islamic Republic of)`
  },
  'IQ': {
    ext: 964,
    img: 'iq.png',
    label: `Iraq`
  },
  'IE': {
    ext: 353,
    img: 'ie.png',
    label: `Ireland`
  },
  'IM': {
    ext: 44,
    img: 'im.png',
    label: `Isle of Man`
  },
  'IL': {
    ext: 972,
    img: 'il.png',
    label: `Israel`
  },
  'IT': {
    ext: 39,
    img: 'it.png',
    label: `Italy`
  },
  'JM': {
    ext: 1876,
    img: 'jm.png',
    label: `Jamaica`
  },
  'JP': {
    ext: 81,
    img: 'jp.png',
    label: `Japan`
  },
  'JE': {
    ext: 44,
    img: 'je.png',
    label: `Jersey`
  },
  'JO': {
    ext: 962,
    img: 'jo.png',
    label: `Jordan`
  },
  'KZ': {
    ext: 7,
    img: 'kz.png',
    label: `Kazakhstan`
  },
  'KE': {
    ext: 254,
    img: 'ke.png',
    label: `Kenya`
  },
  'KI': {
    ext: 686,
    img: 'ki.png',
    label: `Kiribati`
  },
  'KW': {
    ext: 965,
    img: 'kw.png',
    label: `Kuwait`
  },
  'KG': {
    ext: 996,
    img: 'kg.png',
    label: `Kyrgyzstan`
  },
  'LA': {
    ext: 856,
    img: 'la.png',
    label: `Lao People's Democratic Republic`
  },
  'LV': {
    ext: 371,
    img: 'lv.png',
    label: `Latvia`
  },
  'LB': {
    ext: 961,
    img: 'lb.png',
    label: `Lebanon`
  },
  'LS': {
    ext: 266,
    img: 'ls.png',
    label: `Lesotho`
  },
  'LR': {
    ext: 231,
    img: 'lr.png',
    label: `Liberia`
  },
  'LY': {
    ext: 218,
    img: 'ly.png',
    label: `Libya`
  },
  'LI': {
    ext: 423,
    img: 'li.png',
    label: `Liechtenstein`
  },
  'LT': {
    ext: 370,
    img: 'lt.png',
    label: `Lithuania`
  },
  'LU': {
    ext: 352,
    img: 'lu.png',
    label: `Luxembourg`
  },
  'MG': {
    ext: 261,
    img: 'mg.png',
    label: `Madagascar`
  },
  'MW': {
    ext: 265,
    img: 'mw.png',
    label: `Malawi`
  },
  'MY': {
    ext: 60,
    img: 'my.png',
    label: `Malaysia`
  },
  'MV': {
    ext: 960,
    img: 'mv.png',
    label: `Maldives`
  },
  'ML': {
    ext: 223,
    img: 'ml.png',
    label: `Mali`
  },
  'MT': {
    ext: 356,
    img: 'mt.png',
    label: `Malta`
  },
  'MH': {
    ext: 692,
    img: 'mh.png',
    label: `Marshall Islands`
  },
  'MQ': {
    ext: 596,
    img: 'mq.png',
    label: `Martinique`
  },
  'MR': {
    ext: 222,
    img: 'mr.png',
    label: `Mauritania`
  },
  'MU': {
    ext: 230,
    img: 'mu.png',
    label: `Mauritius`
  },
  'YT': {
    ext: 262,
    img: 'yt.png',
    label: `Mayotte`
  },
  'MX': {
    ext: 52,
    img: 'mx.png',
    label: `Mexico`
  },
  'FM': {
    ext: 691,
    img: 'fm.png',
    label: `Micronesia (Federated States of)`
  },
  'MC': {
    ext: 377,
    img: 'mc.png',
    label: `Monaco`
  },
  'MN': {
    ext: 976,
    img: 'mn.png',
    label: `Mongolia`
  },
  'ME': {
    ext: 382,
    img: 'me.png',
    label: `Montenegro`
  },
  'MS': {
    ext: 1664,
    img: 'ms.png',
    label: `Montserrat`
  },
  'MA': {
    ext: 212,
    img: 'ma.png',
    label: `Morocco`
  },
  'MZ': {
    ext: 258,
    img: 'mz.png',
    label: `Mozambique`
  },
  'MM': {
    ext: 95,
    img: 'mm.png',
    label: `Myanmar`
  },
  'NA': {
    ext: 264,
    img: 'na.png',
    label: `Namibia`
  },
  'NR': {
    ext: 674,
    img: 'nr.png',
    label: `Nauru`
  },
  'NP': {
    ext: 977,
    img: 'np.png',
    label: `Nepal`
  },
  'NL': {
    ext: 31,
    img: 'nl.png',
    label: `Netherlands`
  },
  'NC': {
    ext: 687,
    img: 'nc.png',
    label: `New Caledonia`
  },
  'NZ': {
    ext: 64,
    img: 'nz.png',
    label: `New Zealand`
  },
  'NI': {
    ext: 505,
    img: 'ni.png',
    label: `Nicaragua`
  },
  'NE': {
    ext: 227,
    img: 'ne.png',
    label: `Niger`
  },
  'NG': {
    ext: 234,
    img: 'ng.png',
    label: `Nigeria`
  },
  'NU': {
    ext: 683,
    img: 'nu.png',
    label: `Niue`
  },
  'NF': {
    ext: 672,
    img: 'nf.png',
    label: `Norfolk Island`
  },
  'MP': {
    ext: 1670,
    img: 'mp.png',
    label: `Northern Mariana Islands`
  },
  'NO': {
    ext: 47,
    img: 'no.png',
    label: `Norway`
  },
  'OM': {
    ext: 968,
    img: 'om.png',
    label: `Oman`
  },
  'PK': {
    ext: 92,
    img: 'pk.png',
    label: `Pakistan`
  },
  'PW': {
    ext: 680,
    img: 'pw.png',
    label: `Palau`
  },
  'PA': {
    ext: 507,
    img: 'pa.png',
    label: `Panama`
  },
  'PG': {
    ext: 675,
    img: 'pg.png',
    label: `Papua New Guinea`
  },
  'PY': {
    ext: 595,
    img: 'py.png',
    label: `Paraguay`
  },
  'PE': {
    ext: 51,
    img: 'pe.png',
    label: `Peru`
  },
  'PH': {
    ext: 63,
    img: 'ph.png',
    label: `Philippines`
  },
  'PN': {
    ext: 870,
    img: 'pn.png',
    label: `Pitcairn`
  },
  'PL': {
    ext: 48,
    img: 'pl.png',
    label: `Poland`
  },
  'PT': {
    ext: 351,
    img: 'pt.png',
    label: `Portugal`
  },
  'PR': {
    ext: 1,
    img: 'pr.png',
    label: `Puerto Rico`
  },
  'QA': {
    ext: 974,
    img: 'qa.png',
    label: `Qatar`
  },
  'KR': {
    ext: 82,
    img: 'kr.png',
    label: `Republic of Korea`
  },
  'MD': {
    ext: 373,
    img: 'md.png',
    label: `Republic of Moldova`
  },
  'RO': {
    ext: 40,
    img: 'ro.png',
    label: `Romania`
  },
  'RU': {
    ext: 7,
    img: 'ru.png',
    label: `Russian Federation`
  },
  'RW': {
    ext: 250,
    img: 'rw.png',
    label: `Rwanda`
  },
  'RE': {
    ext: 262,
    img: 're.png',
    label: `Réunion`
  },
  'BL': {
    ext: 590,
    img: 'bl.png',
    label: `Saint Barthélemy`
  },
  'SH': {
    ext: 290,
    img: 'sh.png',
    label: `Saint Helena`
  },
  'KN': {
    ext: 1869,
    img: 'kn.png',
    label: `Saint Kitts and Nevis`
  },
  'LC': {
    ext: 1758,
    img: 'lc.png',
    label: `Saint Lucia`
  },
  'MF': {
    ext: 590,
    img: 'mf.png',
    label: `Saint Martin (French Part)`
  },
  'PM': {
    ext: 508,
    img: 'pm.png',
    label: `Saint Pierre and Miquelon`
  },
  'VC': {
    ext: 1784,
    img: 'vc.png',
    label: `Saint Vincent and the Grenadines`
  },
  'WS': {
    ext: 685,
    img: 'ws.png',
    label: `Samoa`
  },
  'SM': {
    ext: 378,
    img: 'sm.png',
    label: `San Marino`
  },
  'ST': {
    ext: 239,
    img: 'st.png',
    label: `Sao Tome and Principe`
  },
  'SA': {
    ext: 966,
    img: 'sa.png',
    label: `Saudi Arabia`
  },
  'SN': {
    ext: 221,
    img: 'sn.png',
    label: `Senegal`
  },
  'RS': {
    ext: 381,
    img: 'rs.png',
    label: `Serbia`
  },
  'SC': {
    ext: 248,
    img: 'sc.png',
    label: `Seychelles`
  },
  'SL': {
    ext: 232,
    img: 'sl.png',
    label: `Sierra Leone`
  },
  'SG': {
    ext: 65,
    img: 'sg.png',
    label: `Singapore`
  },
  'SX': {
    ext: 1721,
    img: 'sx.png',
    label: `Sint Maarten (Dutch part)`
  },
  'SK': {
    ext: 421,
    img: 'sk.png',
    label: `Slovakia`
  },
  'SI': {
    ext: 386,
    img: 'si.png',
    label: `Slovenia`
  },
  'SB': {
    ext: 677,
    img: 'sb.png',
    label: `Solomon Islands`
  },
  'SO': {
    ext: 252,
    img: 'so.png',
    label: `Somalia`
  },
  'ZA': {
    ext: 27,
    img: 'za.png',
    label: `South Africa`
  },
  'GS': {
    ext: 500,
    img: 'gs.png',
    label: `South Georgia and the South Sandwich Islands`
  },
  'SS': {
    ext: 211,
    img: 'ss.png',
    label: `South Sudan`
  },
  'ES': {
    ext: 34,
    img: 'es.png',
    label: `Spain`
  },
  'LK': {
    ext: 94,
    img: 'lk.png',
    label: `Sri Lanka`
  },
  'PS': {
    ext: 970,
    img: 'ps.png',
    label: `State of Palestine`
  },
  'SD': {
    ext: 249,
    img: 'sd.png',
    label: `Sudan`
  },
  'SR': {
    ext: 597,
    img: 'sr.png',
    label: `Suriname`
  },
  'SJ': {
    ext: 47,
    img: 'sj.png',
    label: `Svalbard and Jan Mayen Islands`
  },
  'SE': {
    ext: 46,
    img: 'se.png',
    label: `Sweden`
  },
  'CH': {
    ext: 41,
    img: 'ch.png',
    label: `Switzerland`
  },
  'SY': {
    ext: 963,
    img: 'sy.png',
    label: `Syrian Arab Republic`
  },
  'TJ': {
    ext: 992,
    img: 'tj.png',
    label: `Tajikistan`
  },
  'TH': {
    ext: 66,
    img: 'th.png',
    label: `Thailand`
  },
  'MK': {
    ext: 389,
    img: 'mk.png',
    label: `The former Yugoslav Republic of Macedonia`
  },
  'TL': {
    ext: 670,
    img: 'tl.png',
    label: `Timor-Leste`
  },
  'TG': {
    ext: 228,
    img: 'tg.png',
    label: `Togo`
  },
  'TK': {
    ext: 690,
    img: 'tk.png',
    label: `Tokelau`
  },
  'TO': {
    ext: 676,
    img: 'to.png',
    label: `Tonga`
  },
  'TT': {
    ext: 1868,
    img: 'tt.png',
    label: `Trinidad and Tobago`
  },
  'TN': {
    ext: 216,
    img: 'tn.png',
    label: `Tunisia`
  },
  'TR': {
    ext: 90,
    img: 'tr.png',
    label: `Turkey`
  },
  'TM': {
    ext: 993,
    img: 'tm.png',
    label: `Turkmenistan`
  },
  'TC': {
    ext: 1649,
    img: 'tc.png',
    label: `Turks and Caicos Islands`
  },
  'TV': {
    ext: 688,
    img: 'tv.png',
    label: `Tuvalu`
  },
  'TW': {
    ext: 886,
    img: 'tw.png',
    label: `Taiwan`
  },
  'UG': {
    ext: 256,
    img: 'ug.png',
    label: `Uganda`
  },
  'UA': {
    ext: 380,
    img: 'ua.png',
    label: `Ukraine`
  },
  'AE': {
    ext: 971,
    img: 'ae.png',
    label: `United Arab Emirates`
  },
  'GB': {
    ext: 44,
    img: 'gb.png',
    label: `United Kingdom of Great Britain and Northern Ireland`
  },
  'TZ': {
    ext: 255,
    img: 'tz.png',
    label: `United Republic of Tanzania`
  },
  'VI': {
    ext: 1340,
    img: 'vi.png',
    label: `United States Virgin Islands`
  },
  'US': {
    ext: 1,
    img: 'us.png',
    label: `United States of America`
  },
  'UY': {
    ext: 598,
    img: 'uy.png',
    label: `Uruguay`
  },
  'UZ': {
    ext: 998,
    img: 'uz.png',
    label: `Uzbekistan`
  },
  'VU': {
    ext: 678,
    img: 'vu.png',
    label: `Vanuatu`
  },
  'VE': {
    ext: 58,
    img: 've.png',
    label: `Venezuela (Bolivarian Republic of)`
  },
  'VN': {
    ext: 84,
    img: 'vn.png',
    label: `Viet Nam`
  },
  'WF': {
    ext: 681,
    img: 'wf.png',
    label: `Wallis and Futuna Islands`
  },
  'EH': {
    ext: 212,
    img: 'eh.png',
    label: `Western Sahara`
  },
  'YE': {
    ext: 967,
    img: 'ye.png',
    label: `Yemen`
  },
  'ZM': {
    ext: 260,
    img: 'zm.png',
    label: `Zambia`
  },
  'ZW': {
    ext: 263,
    img: 'zw.png',
    label: `Zimbabwe`
  },
  'AX': {
    ext: 358,
    img: 'ax.png',
    label: `Åland Islands`
  }
};

export default countries;