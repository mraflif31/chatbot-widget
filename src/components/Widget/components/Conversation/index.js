import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Header from './components/Header';
import Messages from './components/Messages';
import Login from './components/Login';
import Sender from './components/Sender';
import Powered from './components/Powered';
import './style.scss';

const Conversation = props => {
  const isLoggedIn = props.logged;
  
  return (
    <div className="rw-conversation-container">
      <Header
        title={props.title}
        subtitle={props.subtitle}
        toggleChat={props.toggleChat}
        toggleFullScreen={props.toggleFullScreen}
        fullScreenMode={props.fullScreenMode}
        showCloseButton={props.showCloseButton}
        showFullScreenButton={props.showFullScreenButton}
        connected={props.connected}
        logged={props.logged}
        connectingText={props.connectingText}
        closeImage={props.closeImage}
        profileAvatar={props.profileAvatar}
      />
      {!isLoggedIn && (
        <Login disabledInput={props.disabledInput}/>
      )}
      {isLoggedIn && (
        <Messages
          profileAvatar={props.profileAvatar}
          params={props.params}
          customComponent={props.customComponent}
          showMessageDate={props.showMessageDate}
          kgs={props.kgs}
        />  
      )}
      {isLoggedIn && (
        <Sender
          sendMessage={props.sendMessage}
          disabledInput={props.disabledInput}
          connected={props.connected}
        />
      )}
      <Powered />
    </div>
  );
}

const mapStateToProps = state => ({
  logged: state.behavior.get('logged')
});

Conversation.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  sendMessage: PropTypes.func,
  profileAvatar: PropTypes.string,
  toggleFullScreen: PropTypes.func,
  fullScreenMode: PropTypes.bool,
  toggleChat: PropTypes.func,
  showCloseButton: PropTypes.bool,
  showFullScreenButton: PropTypes.bool,
  disabledInput: PropTypes.bool,
  params: PropTypes.object,
  connected: PropTypes.bool,
  connectingText: PropTypes.string,
  closeImage: PropTypes.string,
  customComponent: PropTypes.func,
  showMessageDate: PropTypes.oneOfType([PropTypes.bool, PropTypes.func])
};

export default connect(mapStateToProps)(Conversation);
