import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Send from 'assets/send_button';
import Emoji from 'assets/emoji_button';
import Attach from 'assets/attach_button';
import './style.scss';

const Sender = ({ sendMessage, inputTextFieldHint, disabledInput, connected, userInput }) => {
  const [inputValue, setInputValue] = useState('');

  function handleChange(e) {
    setInputValue(e.target.innerText);
  }

  function handleSubmit(e) {
    sendMessage(e);
    resetInput();
  }

  function resetInput() {
    setInputValue('');
    document.getElementById('rw-sender-textarea').innerText = ''
  }

  function handleKeyDown(e) {
    if (e.key === 'Enter') {
      e.preventDefault();

      if (inputValue && inputValue.length > 0) {
        document.getElementById('rw-sender-button').click();
        document.getElementById('rw-sender-textarea').focus();
      }
    }
  }

  return (
    userInput === 'hide' ? <div /> : (
      <form id="rw-send-form" className="rw-sender" onSubmit={handleSubmit}>
        <button type="button" className="rw-send">
          <Attach className="rw-send-icon" alt="attach" />
        </button>
        <button type="button" className="rw-send">
          <Emoji className="rw-send-icon" alt="emoji" />
        </button>
        <div class="rw-new-message-container">
          {/* <textarea onChange={handleChange} className="rw-new-message" name="message" placeholder={inputTextFieldHint} disabled={disabledInput || !connected || userInput === 'disable'} autoFocus autoComplete="off" /> */}
          <span id="rw-sender-textarea" class="rw-sender-textarea" role="textbox" onInput={handleChange} onKeyDown={handleKeyDown} name="message" placeholder={inputTextFieldHint} disabled={disabledInput || !connected || userInput === 'disable'} autoFocus autoComplete="off" contentEditable></span>
          <input type="text" onChange={handleChange} value={inputValue} className="rw-new-message" name="message" placeholder={inputTextFieldHint} disabled={disabledInput || !connected || userInput === 'disable'} autoFocus autoComplete="off" />
          <button id="rw-sender-button" type="submit" className="rw-send" disabled={!(inputValue && inputValue.length > 0)}>
            <Send className="rw-send-icon" ready={!!(inputValue && inputValue.length > 0)} alt="send" />
          </button>
        </div>
      </form>));
};
const mapStateToProps = state => ({
  inputTextFieldHint: state.behavior.get('inputTextFieldHint'),
  userInput: state.metadata.get('userInput')
});

Sender.propTypes = {
  sendMessage: PropTypes.func,
  inputTextFieldHint: PropTypes.string,
  disabledInput: PropTypes.bool,
  connected: PropTypes.bool,
  userInput: PropTypes.string
};

export default connect(mapStateToProps)(Sender);
