import React, { PureComponent } from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { emitUserMessage, addUserMessage, addResponseMessage, deleteLastMessage } from 'actions';
// import { deleteLastMessage, emitKgPending, toggleInputDisabled, emitCancel } from 'actions';

import './styles.scss';

class Button extends PureComponent {
    constructor(props) {
        super(props);
        // this.handleButtonClick = this.handleButtonClick.bind(this);

        this.state = {
            clicked: false,
        };
    }

    modifyText = (text) => {
        let tempText = text;
        tempText = tempText.replace('oembed', 'iframe')
        tempText = tempText.replace('url', 'src')
        tempText = tempText.replace('watch?v=', 'embed/')
        tempText = tempText.replace('oembed', 'iframe')
        return tempText
    }

    handleButtonClick(text, resp, timestamp) {
        const {
            chooseReply
        } = this.props;

        this.setState({ clicked: true });

        // handleSelect(kg, inputDisabled);
        chooseReply(text, resp, timestamp);
    }

    render() {
        let elements = this.props.message.get('elements');
        // console.log('texxxxxt ', elements);
        if (!elements.button) {
            // console.log('inai')
            elements = elements.toJS()
        } 
        const buttons = elements.button;
        const {timestamp, text} = buttons;
        // const text = buttons.text;

        let textModified = text;


        if (textModified.includes('<iframe') || textModified.includes('<oembed')) {
            textModified = this.modifyText(textModified)
        }

        return (
            <div
                className="rw-response rw-buttons"
            >
                <div
                    className="rw-message-text"
                >
                    <div style={{ whiteSpace: "pre-line" }} dangerouslySetInnerHTML={{ __html: textModified }} />
                </div>
                {!this.state.clicked &&
                    <div className="rw-scenario-buttons-container">
                        {buttons.chatbot_response.response.node_struct.choices.map((elem) => {
                            if (elem.button) {
                                return (
                                    <button onClick={() => this.handleButtonClick(elem.alt_texts[0], buttons.text, timestamp)} type="button" className="rw-scenario-buttons">
                                        {elem.shown_text}
                                    </button>
                                )
                            }
                        })}
                    </div>
                }
            </div>
        );
    };
};

const mapDispatchToProps = dispatch => ({
    chooseReply: (payload, resp, timestamp) => {
        dispatch(deleteLastMessage());
        dispatch(addResponseMessage(resp, timestamp));
        dispatch(emitUserMessage(payload));
        dispatch(addUserMessage(payload));
    }
});

export default connect(null, mapDispatchToProps)(Button);
