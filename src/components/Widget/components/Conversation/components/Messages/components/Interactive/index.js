import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteLastMessage, emitKgPending, toggleInputDisabled, emitCancel } from 'actions';

import './styles.scss';

class Interactive extends PureComponent {
    constructor(props) {
        super(props);
        this.handleKgButton = this.handleKgButton.bind(this);
        this.handleCancelClick = this.handleCancelClick.bind(this);

        this.state = {
            selectedKG: null,
            loading: false,
        };
      }

      handleKgButton(inputDisabled, kg) {
        const {
            handleSelect
        } = this.props;
    
        this.setState({ loading: true });

        handleSelect(kg, inputDisabled);
    }

    handleCancelClick(inputDisabled, emit = true) {
        const {
            handleCancel,
            handleCancelWithoutEmit
        } = this.props;

        if (emit) {
            handleCancel(inputDisabled);
        } else {
            handleCancelWithoutEmit(inputDisabled);
        }
    }

  render() {
      const knowledge_groups = this.props.knowledge_groups;
    //   console.log('interactive knowledge groups ', knowledge_groups)
    // }
    if (knowledge_groups != null) {
        return (
            <>
                {this.state.loading &&
                    <div
                        className="rw-response rw-interactive rw-interactive-loading"
                    >
                        <div className="rw-interactive-spinner-wrapper">
                            <span>Please wait for agent</span>
                            <div id="wave">
                                <span className="rw-dot" />
                                <span className="rw-dot" />
                                <span className="rw-dot" />
                            </div>
                        </div>
                        <br/>
                        <button onClick={() => this.handleCancelClick(false)} type="button" className="rw-cancel-button">
                            Cancel
                        </button>
                    </div>
                }
                {!this.state.loading &&
                    <div
                        className="rw-response rw-interactive"
                    >
                        <div className="rw-markdown">
                            You will be directed to the Agent.<br/>
                            Please select the Knowledge Group below.
                        </div>
                        <br/>
                        <div className="rw-kg-buttons-container">
                            {knowledge_groups.knowledge_groups.map((kg) => {
                                return(
                                    <button onClick={() => this.handleKgButton(true, kg.id)} type="button" className="rw-kg-buttons">
                                        {kg.name}
                                    </button>
                                )
                            })}
                        </div>
                    </div>
                }
            </>
        );
    } else {
        return (
            <div
                className="rw-response rw-interactive"
            ></div>
        )
    }
  }
};

Interactive.propTypes = {
  knowledge_groups: PropTypes.arrayOf(PropTypes.object)
};

const mapDispatchToProps = dispatch => ({
    handleSelect: (kgName, disabled) => {
        dispatch(emitKgPending(kgName));
    },
    handleCancel: (disabled) => {
        dispatch(emitCancel());
        dispatch(deleteLastMessage());
        dispatch(toggleInputDisabled(disabled));
    },
    handleCancelWithoutEmit: (disabled) => {
        dispatch(deleteLastMessage());
        dispatch(toggleInputDisabled(disabled));
    },
  });

export default connect(null, mapDispatchToProps)(Interactive);
