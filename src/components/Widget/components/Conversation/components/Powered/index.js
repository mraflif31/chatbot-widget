import React, { useState } from 'react';
import prosaLogo from 'assets/logo-color.png';

import './styles.scss';

const Powered = () => {
  return (
    <div className="rw-logo-container">
      <a href="https://www.prosa.ai" target="_blank" className="rw-logo-a">
        <span className="rw-logo-caption">Powered by</span>
        <img src={prosaLogo} className="rw-logo-image" alt="prosa logo" />
      </a>
    </div>
  );
};

export default Powered;
