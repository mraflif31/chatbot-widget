import React, { useState } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { loginCredential, toggleInputDisabled } from 'actions';
import PropTypes from 'prop-types';
// import PropTypes from 'prop-types';
// import ImmutablePropTypes from 'react-immutable-proptypes';
// import { connect } from 'react-redux';

// import { MESSAGES_TYPES } from 'constants';
// import { Video, Image, Message, Carousel, QuickReply, Interactive, NotificationMessage } from 'messagesComponents';

import './styles.scss';
import countries from 'assets/constanta';

const Login = ({ submit, toggleInput, disabledInput }) => {
  const [nameInputValue, setNameValue] = useState('');
  const [emailInputValue, setEmailValue] = useState('');
  const [companyInputValue, setCompanyValue] = useState('');
  const [numberInputValue, setNumberValue] = useState('');
  const [nameErrorValue, setNameErrorValue] = useState('');
  const [emailErrorValue, setEmailErrorValue] = useState('');
  const [companyErrorValue, setCompanyErrorValue] = useState('');
  const [numberErrorValue, setNumberErrorValue] = useState('');
  const [extValue, setExtValue] = useState('+62');
  const [extImgSrc, setImgSrc] = useState("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABDBAMAAACYZb3pAAAAD1BMVEX////VM0bOESbniJP////+7S8OAAAAAnRSTlPU+CChtw0AAAAxSURBVEjH7dWxDQAwCMAwqPoA/z/bG0B0c3bPyYput09OIAiCIMj+XywJQRAE+UEGPWWqAM74SM60AAAAAElFTkSuQmCC");

  // const [inputDisabled, setInputDisabled] = useState(false);
  const codeArray = Object.entries(countries).map(([k, v]) => ({
    value: v.ext,
    label: v.label,
    img: require(`./flags/${v.img}`)
  }))

  function handleNameChange(e) {
    setNameValue(e.target.value);
  }

  function handleEmailChange(e) {
    setEmailValue(e.target.value);
  }

  function handleCompanyChange(e) {
    setCompanyValue(e.target.value);
  }

  function handleNumberChange(e) {
    setNumberValue(e.target.value);
  }

  function handleExtChange(e) {
    setImgSrc(e.img);
    setExtValue(`+${e.value}`);
  }

  function nameError(name) {
    if (name.length === 0) {
      return '*Please enter a name';
    } else if (name.length < 3) {
      return '*Name is too short. Please input 3 - 30 characters';
    } else if (name.length > 30) {
      return '*Name is too long. Please input 3 - 30 characters';
    } else {
      return '';
    }
  }

  function emailError(email) {
    if (email.length === 0) {
      return '*Please enter an email address';
    } else {
      return '';
    }
  }

  function companyError(company) {
    if (company.length === 0) {
      return '*Please enter your company name';
    } else if (company.length < 3) {
      return '*Company name is too short. Please input 3 - 30 characters';
    } else if (company.length > 30) {
      return '*Company name is too long. Please input 3 - 30 characters';
    } else {
      return '';
    }
  }

  function numberError(number) {
    if (number.length === 0) {
      return '*Please input a phone number';
    } else if (number.length < 4 || number.length > 12) {
      return '*Please input 4-12 digits phone number';
    } else if (number.match(/^[0-9]+$/) === null) {
      return '*Please input numbers only';
    } else {
      return '';
    }
  }

  function handleSubmit(e) {
    e.preventDefault();

    const nameTemp = nameError(nameInputValue);
    const emailTemp = emailError(emailInputValue);
    const numberTemp = numberError(numberInputValue);
    const companyTemp = companyError(companyInputValue);

    setNameErrorValue(nameTemp);
    setEmailErrorValue(emailTemp);
    setNumberErrorValue(numberTemp);
    setCompanyErrorValue(companyTemp);

    if (nameTemp === '' && emailTemp === '' && numberTemp === '' && companyTemp === '') {
      // setInputDisabled(true);
      toggleInput(true);
      submit(nameInputValue, emailInputValue, `${extValue}${numberInputValue}`, companyInputValue);
    }
  }

  return (
    <div className="rw-login-container">
      <span className="rw-login-title">We are ready to help you</span>
      <span className="rw-login-subtitle">First, tell us about yourself, please!</span>
      <form className="rw-login-form" onSubmit={handleSubmit} >
        <input className="rw-login-input" type="text" onChange={handleNameChange} placeholder="Enter your name" autoComplete="off" required disabled={disabledInput}/>
        <p class="rw-login-error">{nameErrorValue}</p>
        <input className="rw-login-input" type="email" onChange={handleEmailChange} placeholder="Enter your email" autoComplete="off" required disabled={disabledInput}/>
        <p class="rw-login-error">{emailErrorValue}</p>
        <div className="rw-login-input rw-login-ext-container">
          <img src={extImgSrc} className="rw-login-ext-avatar" alt="ext avatar" />
          <Select maxMenuHeight={200} onChange={handleExtChange} classNamePrefix="rw-login-sel" className="rw-login-ext" options={codeArray} isDisabled={disabledInput}/>
          <span>{extValue}</span>
          <span>
            <input className="rw-login-ext-input" type="tel" onChange={handleNumberChange} autoComplete="off" required disabled={disabledInput}/>
          </span>
        </div>
        <p class="rw-login-error">{numberErrorValue}</p>
        <input className="rw-login-input" type="text" onChange={handleCompanyChange} placeholder="Enter your company name" autoComplete="off" required disabled={disabledInput}/>
        <p class="rw-login-error">{companyErrorValue}</p>
        {/* <button className="rw-login-button" onClick={handleSubmit}> */}
        <button type="submit" className="rw-login-button" disabled={disabledInput}>
          <span>Continue</span>
          <i class="fas fa-long-arrow-alt-right rw-login-arrow-icon"></i>
        </button>
      </form>
    </div>
  );
};

// Login.propTypes = {
//   messages: ImmutablePropTypes.listOf(ImmutablePropTypes.map),
//   profileAvatar: PropTypes.string,
//   customComponent: PropTypes.func,
//   showMessageDate: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
//   displayTypingIndication: PropTypes.bool
// };

// Login.defaultTypes = {
//   displayTypingIndication: false
// };

// export default connect(store => ({
//   messages: store.messages,
//   displayTypingIndication: store.behavior.get('messageDelayed')
// }))(Login);

const mapDispatchToProps = dispatch => ({
  submit: (name, email, phone, company) => {
      dispatch(loginCredential({name, email, phone, company}));
  },
  toggleInput: (input) => {
    dispatch(toggleInputDisabled(input));
  }
});

Login.propTypes = {
  disabledInput: PropTypes.bool
};

export default connect(null, mapDispatchToProps)(Login);
