import io from 'socket.io-client';

export default function (socketUrl, customData, path, protocolOptions) {
  const options = path ? { path } : {};

  // console.log('socketio options')
  // console.log(options)

  options.transports = protocolOptions.transports
  options.transportOptions = protocolOptions.transportOptions

  // console.log('socketio options new')
  // console.log(options)

  const socket = io(socketUrl, options);

  // console.log('socketio')
  // console.log(socketUrl, customData, path, protocolOptions, options)

  socket.on('connect', () => {
    // console.log(`connect:${socket.id}`);
    socket.customData = customData;
  });

  // socket.on('connect_error', (error) => {
  //   console.log(error);
  // });

  // socket.on('error', (error) => {
  //   console.log(error);
  // });

  // socket.on('disconnect', (reason) => {
  //   console.log(reason);
  // });

  return socket;
}
